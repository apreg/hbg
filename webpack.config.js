'use strict';
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var fs = require('fs');
var path = require('path');
var autoprefixer = require('autoprefixer-core');
var simpleExtend = require('postcss-extend');
var atImport = require('postcss-import');

module.exports = {
  output: {
    filename: 'app.js',
    path: './www'
  },

  cache: true,
  debug: true,
  devtool: '#source-map',
  entry: [
    'webpack-dev-server/client?http://localhost:8080', // WebpackDevServer host and port
    'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
    './src/app.js'
  ],

  stats: {
    colors: true,
    reasons: true
  },

  resolve: {
    //to avoid Only a ReactOwner can have refs. It looks like there are two instances of React in place.
    // http://stackoverflow.com/questions/28519287/what-does-only-a-reactowner-can-have-refs-mean
    alias: {
      'react': path.join(__dirname, 'node_modules', 'react')
    },
    packageMains: [
      'webpack',
      'browser',
      'web',
      'browserify',
      ['jam', 'main'],
      'style',
      'main'
    ],
    extensions: ['', '.json', '.js'],
    modulesDirectories: ['node_modules', 'bower_components', 'web_modules', 'components']
  },
  module: {
    preLoaders: [{
      test: '\\.js$',
      exclude: 'node_modules',
      loader: 'jshint'
    }],
    loaders: [
      {
        test: /\.json$/,
        loader: "json-loader"
      }, {
        test: /\.js$/,
        loader: 'babel?stage=0&optional=runtime',//for Object.assign
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader?importLoaders=1!postcss-loader')
      }, {
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192'
      }, {
        test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      }, {
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      },
      {
        test: /\.svg$/,
        loader: "file-loader"
      }
    ]
  },
/*
  node: {
    fs: 'empty',
    net:'empty',
    module:'empty',
    tap: 'empty'

  },
*/
  postcss: [
    atImport({
      path: ['node_modules', './src']
    }),
    autoprefixer,
    simpleExtend
  ],

  plugins: [
    new ExtractTextPlugin('app.css', {allChunks: true}),
	new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};