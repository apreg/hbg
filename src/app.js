'use strict';
import React from 'react';
import App from './containers/App';
import Parse from 'parse';
import ReactDOM from 'react-dom';
require('./styles/app.css');

window.onload = function(){
    var url = document.URL;
    var isSmart = (url.indexOf("http://") === -1 && url.indexOf("https://") === -1);
    if( isSmart ){
        document.addEventListener('deviceready', startApp, false);
    }
    else{
        startApp();
    }
};

function startApp(){
    Parse.initialize("81qKf4TX5x8hPmZMLCEtjBBLTWk7ihdXygwCGYVy", "xM5AGydTHnRkmHHkpJVcgR3FqZuArtds4u4azH0a");
    ReactDOM.render(<App/>, document.getElementById("react-root"));
}