'use strict';
import React from 'react';
//import Icon from 'react-svg-icons';
import Rating from '../components/Rating/Rating';
//import Slider from 'react-slick';
import Parse from 'parse';
import ParseReact from 'parse-react';
import SnapViews from '../components/SnapViews/SnapViews';
//import injectTapEventPlugin from 'react-tap-event-plugin';
import SlidingMenu from '../components/SlidingMenu/SlidingMenu';
import Loader from 'react-loader';

//injectTapEventPlugin();
require('./App.css');
var ParseComponent = ParseReact.Component(React);

export default
class App extends ParseComponent {

  constructor() {
    super();
    this.state = {
      loaded: false,
      currentIndex: 0,
      views: [],
      rateDisabled: true,
      initialRate: 0
    }
    this.handleTouchTap = this.handleTouchTap.bind(this);
    this.toggleMenus = this.toggleMenus.bind(this);
    this.handleSocialSharingClick = this.handleSocialSharingClick.bind(this);
    this.handleSnapped = this.handleSnapped.bind(this);
    this._saveSeen = this._saveSeen.bind(this);
    this._saveRate = this._saveRate.bind(this);
    this._getRate = this._getRate.bind(this);
    this.handleRated = this.handleRated.bind(this);
  }

  observe() {
    return {
      images: (new Parse.Query('Images')).ascending('createdAt').limit(10)
    };
  }

  handleClick = (e) => {
    this.toggleMenus(e);
    console.log("img click");
  }

  handleTouchTap(e) {
    this.toggleMenus(e);
  }

  toggleMenus(e) {

    this.refs.top.toggleState();
    this.refs.bottom.toggleState();

  }

  handleSnapped(currentIndex) {
    this._saveSeen(currentIndex);
    this.setState({currentIndex: currentIndex});
    var rate = this._getRate(currentIndex);
    if (rate) {
      this.setState({initialRate: rate, rateDisabled: true});
    } else {
      this.setState({initialRate: 0, rateDisabled: false});
    }
  }

  _saveSeen(index) {
    var id = this.state.views[index].id;
    var dataString = localStorage.getItem(id);
    if (dataString) {
      var data = JSON.parse(dataString);
      data.seen = true;
      localStorage.setItem(id, JSON.stringify(data));
    } else {
      localStorage.setItem(id, JSON.stringify({seen: true}));//TODO wrap localstorage
    }
  }

  _saveRate(index, rate) {
    var id = this.state.views[index].id;
    var dataString = localStorage.getItem(id);
    if (dataString) {
      var data = JSON.parse(dataString);
      data.rate = rate;
      localStorage.setItem(id, JSON.stringify(data));
    } else {
      localStorage.setItem(id, JSON.stringify({rate: rate}));//it should not bee possible but still
    }
  }

  _getRate(index) {
    var dataString = localStorage.getItem(this.state.views[index].id);
    if (dataString) {
      return JSON.parse(dataString).rate;
    }
    return null;
  }

  _sendRate() {

  }

  handleRated(rate) {
    this._saveRate(this.state.currentIndex, rate);
    //if there is net the nsend it
  }

  handleSocialSharingClick() {
    window.plugins.socialsharing.share(this.state.views[this.state.currentIndex]);
  }

  render() {
    var src = require('../assets/images/csilla_02.jpg');

    var content = ( <img src={src}/>);
    if (this.data.images.length) {
      this.state.views = this.data.images.map(function (o) {

        return {id: o.objectId, url: o.img._url};
      });
      //TODO save first item
    } else if (this.pendingQueries().length) {
      content = (<img src={src}/>);
    }

    var s = {backgroundImage: 'url(' + src + ')'};
    return (
      <Loader loaded={this.pendingQueries().length === 0 }>
        <div onClick={this.handleClick} style={{
        backgroundColor: 'grey', position: 'fixed',
        left: '0',
        right: '0',
        top: '0',
        bottom: '0'
      }}>
          <SlidingMenu ref="top" alignment="Top">
            <button style={{color: 'white', width: '100%'}} onClick={this.handleSocialSharingClick}>share</button>
          </SlidingMenu>

          <SnapViews views={this.state.views} onSnapped={this.handleSnapped}>
          </SnapViews>

          <SlidingMenu ref="bottom" alignment="Bottom">
            <Rating initialRate={this.state.initialRate} onRate={this.handleRated}
                    readonly={this.state.rateDisabled}></Rating>
          </SlidingMenu>
        </div>
      </Loader>
    )
  }
}