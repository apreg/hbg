'use strict';
import React, {Component} from 'react';
import Swipeable from 'react-swipeable';
require('./SnapView.css');

export default class SnapView extends React.Component{

  constructor(props){
    super(props);
    this.state = {left: 0};
  }

  render () {
    return (
      <figure className="SnapView" style={{transform: 'translate3d('+ this.props.left + '%, 0,0)'}}>
        <img src={this.props.view}></img>
      </figure>
    )
  }
}