'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
//import Swipeable from 'react-swipeable';
import Swipeable from '../Swipeable/Swipeable';
import SnapView from './SnapView';
require('./SnapViews.css');

export default
class SnapViews extends React.Component {

  static LEFT = -1;
  static RIGHT = 1;
  static CENTER = 0;

  static propTypes = {
    snapThreshold: React.PropTypes.number,
    onSnapped:  React.PropTypes.func
  }

  static defaultProps = {
    snapThreshold: 0.4
  };

  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
      viewStates: this.initViews(),
      direction: 0,
      actFrameX: 0,
      startFrameX: 0,
      transitionDuration: 0
    };

    this.moveFrame = this.moveFrame.bind(this);
    this.shiftViews = this.shiftViews.bind(this);
    this.initViews = this.initViews.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.handleSwiping = this.handleSwiping.bind(this);
  }

  initViews() {
    var viewStates = [];
    for (let i = 0; i < 3; ++i) {
      viewStates.push({pos: i * 100, content: this.props.views[i].url});
    }
    return viewStates;
  }

  handleResize() {
    var wrapper = ReactDOM.findDOMNode(this.refs.wrapper);
    this.setState({'frameWidth': wrapper.clientWidth, direction: SnapViews.CENTER, transitionDuration: 0});
    this.moveFrame(null, null, false);
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleSwiping(e, pos) {
    var direction = SnapViews.CENTER;
    if (pos.deltaX > 0) {
      direction = SnapViews.LEFT;
    } else if (pos.deltaX < 0) {
      direction = SnapViews.RIGHT;
    }
    this.setState({
      direction: direction,
      actFrameX: this.state.startFrameX - pos.deltaX
    });
    console.log( this.state.startFrameX - pos.deltaX);
  }

  moveFrame(e, pos, isFlick) {
    var startFrameX;
    var nextIndex = this.state.currentIndex - this.state.direction;
    if (isFlick && nextIndex >= 0) {//big enough movement to show different image
      this.shiftViews();
      if (nextIndex < this.props.views.length) {
        startFrameX = -nextIndex * this.state.frameWidth;
        this.setState({
          currentIndex: nextIndex,
          startFrameX: startFrameX,
          actFrameX: startFrameX,
          transitionDuration: 250
        });
      }
      if (this.props.onSnapped){
        this.props.onSnapped(nextIndex);
      }
    } else {//stay on current image
      this.setState({
        actFrameX: -this.state.currentIndex * this.state.frameWidth
      });
    }

  }

  shiftViews() {
    var i = this.state.currentIndex - (this.state.direction * 2);
    //load content
    if (i > this.props.views.length) {//we've run out of views need for a load from server
      //TODO try to load
      //if we get new content then we shift
    } else if (i >= 0) {
      this.state.viewStates[i % 3].pos = i * 100;
      this.state.viewStates[i % 3].content = this.props.views[i].url;
    }
  }

  render() {
    var transition = 'all 250ms ease-out';
    return (
      <div ref="wrapper" name="wrapper" style={{overflow: 'hidden', position: 'relative', height: '100%'}}>
        <Swipeable onSwiped={this.moveFrame}
                   onSwiping={this.handleSwiping}
                   flickThreshold={this.props.snapThreshold}
                   delta={0}>
          <div className="SnapViews" style={{
          transform: 'translate3d(' + this.state.actFrameX + 'px,0,0)',
          transitionTimingFunction: 'ease-out',
          transitionDuration: this.state.transitionDuration + 'ms'
        }}>
            <SnapView left={this.state.viewStates[0].pos} view={this.state.viewStates[0].content}></SnapView>
            <SnapView left={this.state.viewStates[1].pos} view={this.state.viewStates[1].content}></SnapView>
            <SnapView left={this.state.viewStates[2].pos} view={this.state.viewStates[2].content}></SnapView>

          </div>
        </Swipeable>
      </div>
    )
  }
}