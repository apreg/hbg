'use strict';
import React from 'react';
require('./SlidingMenu.css');

export default
class SlidingMenu extends React.Component {
  //ES7+ Property Initializers
  static propTypes = {alignment: React.PropTypes.oneOf(['Top', 'Left', 'Bottom', 'Right'])};//basically an enum

  constructor(props) {
    super(props);
    this.state = {visible: false};
    this.toggleState = this.toggleState.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  toggleState() {
    this.setState({visible: !this.state.visible});
  }
  handleClick(e) {
    e.stopPropagation();
    console.log("click fro mmenu");
  }
  render() {
    var className = "SlidingMenu" + this.props.alignment + (this.state.visible ? ' is-visible' : ' is-hidden');
    return (
      <div className={className} onClick={this.handleClick}>
        {this.props.children}
      </div>
    )
  }
}